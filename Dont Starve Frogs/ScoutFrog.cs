﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public class ScoutFrog:Frog
    {
       
        public void ReadMap()
        {
            Console.WriteLine("Reading map");
        }

        public void Track()
        {
            Console.WriteLine("Tracking");
        }

        override public void Croak()
        {
            Console.WriteLine(".........");
        }

        public override void Hop(double distance)
        {
            Console.WriteLine($"Hopped {distance*2}cm away");
        }

    }
}
