﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public class Vitals
    {
        public double HeartRate { get; set; }
        public double BloodPressure { get; set; }
        public List<string> Allergies { get; set; }

    }
}
