﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public abstract class Frog
    {

        public Frog()
        {
            IsAlive = false;
            Console.WriteLine("A tadpol is born");
            BackPack = new BackPack();
        }

        public Frog(string frogName, int frogAge)
        {
            Name = frogName;
            Age = frogAge;
            IsAlive = true;
            Console.WriteLine("A tadpol is born");
            BackPack = new BackPack();
        }

        //fields
        public int Age { get; set; }
        public bool IsAlive { get; set; }
        public string Name { get; set; }
        public Vitals Vitals { get; set; }
        public BackPack BackPack { get; set; }

        abstract public void Croak();

        public bool CheckIfAlive()
        {
            Console.WriteLine(this.GetType().Name);
            return true;
        }

        public virtual void Hop(double distance)
        {
            Console.WriteLine($"Hopped {distance}cm away");
            
            
        }

      /// <summary>
      /// 
      /// </summary>
      /// <param name="compartment"></param>
      /// <param name="item"></param>
      /// <exception cref="InvalidBackpackItem"></exception>
      /// <returns></returns>
        public bool Pack(BackPackCompartments compartment, Item item)
        {
            if(compartment==BackPackCompartments.MAIN && item.GetType().Name == "Food")
            {
                throw new InvalidBackpackItem("Food cannot go in main compartment");
            }

            BackPack.Contents.Add(compartment, item);

            return true;
        }

    }
}
