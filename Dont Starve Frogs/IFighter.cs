﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public interface IFighter
    {
        public void Fight();

        public string BattleCry();
        
    }
}
