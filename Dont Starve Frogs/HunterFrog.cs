﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public class HunterFrog : Frog, IFighter
    {
        public HunterFrog()
        {
            ButcherSkillLevel = 3;
           
        }

        public HunterFrog(int butcherSkillLvl, string name, int age):base(name,age)
        {
            ButcherSkillLevel = butcherSkillLvl;

        }

        public int ButcherSkillLevel { get; set; }
        public List<string> WeaponProficiencies { get; set; }

        public void Hunt()
        {
            Console.WriteLine("hunting");
        }

        public void Butcher()
        {
            Console.WriteLine("Butchering");
        }

        public void Preserve()
        {
            Console.WriteLine("preserving");
        }

        override public void Croak()
        {
            Console.WriteLine("GRAAROAOORAORRBIITTE!!!!!");
        }

        public void Fight()
        {
            Croak();
            Console.WriteLine("I will fight anyone.....smaller than me");
        }

        public string BattleCry()
        {
            return "GRAAROAOORAORRBIITTE!!!!!  watahthaaaaAAAHAHAH";
        }

        //build a tent
    }
}
