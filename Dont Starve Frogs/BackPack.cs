﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public class BackPack
    {
        public Dictionary<BackPackCompartments, Item> Contents { get; set; } 

        
        public BackPack()
        {
          Contents  = new Dictionary<BackPackCompartments, Item>();
        }
    }
}
