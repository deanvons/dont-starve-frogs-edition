﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public class InvalidBackpackItem:Exception
    {
        public InvalidBackpackItem()
        {
           
        }

        public InvalidBackpackItem(string message) : base(message)
        {
        }

        public InvalidBackpackItem(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidBackpackItem(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
