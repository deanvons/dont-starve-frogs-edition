﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dont_Starve_Frogs
{
    public enum BackPackCompartments
    {
        MAIN,
        SMALL,
        MEDIUM,
        MEDIUM_UTILITY
    }
}
