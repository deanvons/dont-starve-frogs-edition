﻿using Dont_Starve_Frogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Dont__Starve_Frogs_Tests
{
    public class FrogTests
    {

        [Fact]
        public void BattleCry_AskedToBattleCry_ReturnValidMessage()
        {
            // Arrange
            string expected = "GRAAROAOORAORRBIITTE!!!!!  watahthaaaaAAAHAHAH";
            HunterFrog hunterFrogObj = new HunterFrog();

            // Act
            string actual = hunterFrogObj.BattleCry();

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Frog_WhenCreated_isAliveSetToTrue()
        {
            // Arrange
            bool expected = true;
            
            // Act
            ShamanFrog frog = new ShamanFrog();
            bool actual = true;

            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Pack_AddAFoodItemToMain_ShouldThrowInvalidItemException()
        {

            // Arrange
            Food food = new Food();
            HunterFrog hunterFrog = new HunterFrog();
            BackPackCompartments compartment = BackPackCompartments.MAIN;

            // Assert
            Assert.Throws<InvalidBackpackItem>(() => hunterFrog.Pack(compartment, food));
        }


    }
}
